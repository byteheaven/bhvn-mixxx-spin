# bhvn-mixxx-spin

Vestax Spin mapping for Mixxx [BHVN Official]

# Usage

Here’s how it works, barring bugs and your hacks. Controls that are not listed 
work in the default boring way!

To get it: download the "sources" zip file. The midi and script files are included.
Put these in your controller scripts dir and use them.

## Transport

* Play: works normally
* Cue Set: acts as a normal CDJ cue button
* Cue Jump: slip-reverse(censor)

Shifted, these work just like the silkscreen says: jump to start, rewind, ff.

## Sync

* Sync: tries to work like modern Mixxx
* Sync-longpress: sets this deck as master
* Plus: beatjump forward
* Minus: beatjump backward
* Shift:Sync: Match key
* Shift:Sync-longpress: reset track key
* Shift:Plus: Key-up (halfstep)
* Shift:Minus: Key-down

## Loop

* Loop-Auto: Create/move beatloop
* Loop-In/Out: If loop is active, clears loop. Otherwise, first click sets loop-in, second sets loop-out
* Shift:Loop-Auto: double beatloop size (does nothing to manual loop)
* Shift:Loop-In/Out: halve beatloop size
* FX: Hotcue 1
* Shift:FX: Clear Hotcue 1

Manual looping is almost never useful unless the beatgrid is completely wrong, 
and I can’t remember the last time I used it. Perhaps a reloop-focused use for 
the In/Out button would be more useful?

## Navigation

* Up&Down: set selection to previous/next
* Shift:Up&Down: Page-up and Page-down
* Left&Right: load selection into left or right deck
* Shift:Left&Right: same as Tab and Shift-Tab navigation

## Mic Knobs
The Mic FX and Pitch knobs control the left and right Super knob respectively.

## AutoDJ
Hey, we all have to pee sometimes.

* Automix: toggles AutoDJ
* Shift:Automix: triggers “fade now” in AutoDJ
* Mic OnOff: add current selection to end of AutoDJ playlist
* Shift:Mic OnOff: add current selection next in AutoDJ Playlist

## Record button
I mapped it to split-cue toggle. This is not as useful as you might think, 
because there is no headphone blend control knob, and you usually want the blend
in the middle for split-cue. It does nothing fancy so it should be easy to 
remap to Record or Broadcast if that’s your kind of thing. I prefer an external 
record/broadcast rig myself.
