// Script support for Mixxx -> Vestax Spin mapping
// Version 1.3.1
// Copyright Tom Surace: 2020
// Licensed under the WTFPL - share and enjoy.
// Bugfixes, altnernate mappings, and enhancement feedback welcomed.
// Attribution appreciated.


// -- Configuration and constants

// Scratch tuning
//
const SCRATCH_ALPHA = 1.0 / 8;
const SCRATCH_BETA = SCRATCH_ALPHA / 32;

/**
 * Jog wheel correction. Mixxx is reference to 256/rev [cn], and
 * this wheel turns 375/rev [cn].
 */
const JOG_CORRECTION = 256 / 375;

const DECK_LEDS = [
    0x20, 0x21, 0x24, 0x25, 0x29, 0x2A, 0x2B, 0x2C,
    0x2D, 0x32, 0x35, 0x33, 0x42, 0x46
];

const CH3_LEDS = [
    0x26, 0x28, 0x29, 0x2A
];


// -- TODO: should LED be in a reusable library?

/**
 * A simple led that can be turned on or off
 *
 * @param channel is the 0-indexed midi channel on which we will send.
 *   0 = channel 1, etc.
 * @param ctrl controller ID for LED control byte
 */
const LED = function(channel, ctrl) {
    /**
     * Status byte to send. It's a note-on/off message so...
     */
    this.status = 0x90 + channel;
    this.ctrl = ctrl;
    this.isOn = false;

    /**
     * If set to false, the LED will continue to track state,
     * but not send MIDI on state changes.
     */
    this.isActive = true;
};

/**
 * flush value to MIDI immediately, regardless of isActive
 */
LED.prototype.send = function() {
    var value;
    if (this.isOn) {
        value = 0x7f;  // Pretty much all LEDs on this controller work the same
    }
    else {
        value = 0x00;
    }

    midi.sendShortMsg(this.status, this.ctrl, value);
};


/**
 * Send an "off" signal now, regardless of the current internal state
 */
LED.prototype.sendLightsOut = function() {
    midi.sendShortMsg(this.status, this.ctrl, 0);
};

/**
 * Set LED to new value and possibly update control surface. Value
 * is send if isActive === true.
 *
 * @param value boolean value: true for on.
 */
LED.prototype.set = function(value) {
    if (this.isOn !== value) {
        this.isOn = value;
        if (this.isActive) {
            this.send();
        } 
    }
}

/**
 * Toggles the state of an LED, and returns the new state
 */
LED.prototype.toggle = function() {
    this.isOn = !this.isOn;
    if (this.isActive) {
        this.send();
    }

    return this.isOn;
};


/**
 * Extend LED to be a tempo-based blinky by hooking
 * into the beat_active signal for the indicated
 * deck group
 */
const TempoLED = function(channel, control, group) {
    LED.call(this, channel, control);

    this.group = group;

    /**
     * If counter > 0, is either on or waiting to become active
     */
    this._counter = 0;
};

TempoLED.prototype = Object.create(LED.prototype);

//
// Aim for a 40ms flash starting on the beat.
// Try to compensate for MIDI-out lag as well.
//
// These timings are tuned specifically to the SPIN's slow
// USB on a system that is borderline. :)
//
TempoLED.TICK_MS = 40;       // Tick speed
TempoLED.INITIAL_TICKS = 4;  // Delay = INITIAL - BLINK - 1
TempoLED.BLINK_TICKS = 2;    // stays on for this many ticks

// Animate all TempoLEDs using a single timer for efficiency

/**
 * Array of all TempoLEDs, for animation.
 */
TempoLED._ledRegistry = [];
TempoLED._globalTick = function() {
    for (var i = 0; i < TempoLED._ledRegistry.length; i++) {
	TempoLED._ledRegistry[i].tick();
    }
};

TempoLED._animationTimer = engine.beginTimer(TempoLED.TICK_MS, function() {
        TempoLED._globalTick();
    },
    false /* one-shot */ );

TempoLED.prototype.beatActive = function(value, group, controlName) {
    if (value === 1) {
        this._counter = TempoLED.INITIAL_TICKS;
    }
};

/**
 * Animation timer callback.
 */
TempoLED.prototype.tick = function() {
    if (!this.isActive) {  // Don't blink while inactive!
        return;
    }

    if (this._counter === 0) {
        return;
    }

    -- this._counter;

    // do a "this.send" but with the inverse value while beat_active.
    // The sent value is not stored.

    var isOn = this.isOn;
    if (this._counter > TempoLED.BLINK_TICKS) { // don't blink yet!
        return;
    }
    else if (this._counter > 0) {               // Within the blink window?
        isOn = !isOn;
    }
    else {                                      // Blink is ended!
        ; // do nothing
    }

    var value;
    if (isOn) {
        value = 0x7f;
    }
    else {
        value = 0x00;
    }

    midi.sendShortMsg(this.status, this.ctrl, value);
};

TempoLED.prototype.init = function() {
    engine.makeConnection(this.group, 'beat_active', this.beatActive);

    TempoLED._ledRegistry.push(this);
};

const VuController = function(group, channel) {
    this.group = group;

    /**
     * If truthy, the peak meter is lit.
     */
    this.isPeaking = 0;

    this.led1 = new LED(channel, 0x29);
    this.led2 = new LED(channel, 0x2a);
    this.led3 = new LED(channel, 0x2b);
    this.led4 = new LED(channel, 0x2c);
    this.led5 = new LED(channel, 0x2d);
};

VuController.prototype.init = function() {
    engine.makeConnection(this.group, 'VuMeter', this.handleVu);
    engine.makeConnection(this.group, 'PeakIndicator', this.handlePeak);
}

VuController.prototype.handleVu = function(value, group, controlName) { 

    // When peaking, save current values but don't send

    if (this.isPeaking) {
        this.led1.isOn = value >= 1/6;
        this.led2.isOn = value >= 2/6;
        this.led3.isOn = value >= 3/6;
        this.led4.isOn = value >= 4/6;
        this.led5.isOn = value >= 5/6;
        return;
    }

    // Some sort of "for display" vu value that doesn't seem to be a 
    // genuine "VU" value but also isn't Peak, between 0 and 1.0

    this.led1.set(value >= 1/6);
    this.led2.set(value >= 2/6);
    this.led3.set(value >= 3/6);
    this.led4.set(value >= 4/6);
    this.led5.set(value >= 5/6);
};

VuController.prototype.handlePeak = function(value, group, controlName) { 
    // boolean: value will be 1 when peaking
    this.isPeaking = value;
    this.led5.set(value);
    if (value) {
        // Start peaking: send lights-out for the non-red LEDs 
        // and they'll stop sending data until the peak ends
        this.led1.sendLightsOut();
        this.led2.sendLightsOut();
        this.led3.sendLightsOut();
        this.led4.sendLightsOut();
    }
    else {
        this.led1.send();
        this.led2.send();
        this.led3.send();
        this.led4.send();
        this.led5.send();
    }
};


/**
 * Shared state (shift, mostly)
 */
const GlobalState = function() {
    /**
     * Is the shift button held?
     */
    this.isShifted = false;
};

const Deck = function(globalState, group, deckNumber) {
    // Channel = 0x0 or 0x1
    var channel = deckNumber - 1;

    this.globalState = globalState;
    this.group = group;
    this.deckNumber = deckNumber;

    this.vinylLed = new LED(channel, 0x24);
    this.syncLed = new TempoLED(channel, 0x46, this.group);
    this.inOutLed = new LED(channel, 0x21);

    /**
     * Hold Sync for 1s to become sync master
     */
    this.beatsyncTimer = null;

    /**
     * shift-hold Sync for 1s to reset track pitch
     */
    this.resetPitchTimer = null;
};

Deck.prototype.init = function() {
    this.vinylLed.isOn = true;
    this.vinylLed.send();

    this.syncLed.init();
    engine.makeConnection(this.group, 'sync_mode',
        function(value, group, controlName) { this.syncLed.set(value > 0.5 && value < 2.5); });

    // in-out led lit if loop-in is set
    this.inOutLed.send();
    engine.makeConnection(this.group, 'loop_start_position',
        function(value, group, controlName) { this.inOutLed.set(value >= 0); });
};

Deck.prototype.handlePlay = function(channel, control, value, status, group) {
    if (value !== 0x7f) {
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue(this.group, 'start', 1); // go home!
    }
    else {
        script.toggleControl(this.group, 'play');
    }
}

/**
 * Map the cue "Set" button.
 */
Deck.prototype.handleCueSet = function(channel, control, value, status, group) {

    if (value !== 0x7f) {
        engine.setValue(this.group, 'cue_default', 0);
        engine.setValue(this.group, 'back', 0);
        return;
    }
    
    // Most people are used to a single-button cdj-style CUE controls, which frees 
    // the cue-jump button to be used as, you know, whatever.

    if (this.globalState.isShifted) {
        engine.setValue(this.group, 'back', 1);
    }
    else {
        engine.setValue(this.group, 'cue_default', 1);
    }
};

/**
 * Handler for the cue-jump button. 
 */
Deck.prototype.handleCueJump = function(channel, control, value, status, group) {

    if (value !== 0x7f) {
        engine.setValue(this.group, 'reverseroll', 0);
        engine.setValue(this.group, 'fwd', 0);
        return;
    }

    // We're using the Set button as a generic cue control, so this can
    // be repurposed as... um... whatever

    if (this.globalState.isShifted) {
        engine.setValue(this.group, 'fwd', 1);
    }
    else {
        // alternative: slip_enabled
        engine.setValue(this.group, 'reverseroll', 1);
    }
};

/**
 * You TOUCHED IT!
 */
Deck.prototype.handleWheelTouch = function(channel, control, value, status, group) {
    if (value === 0x7F) {         // If touched
        if (this.vinylLed.isOn) { // and "vinyl mode" is enabled
            engine.scratchEnable(this.deckNumber, 
                                 375, // ticks per revolution
                                 33.3333, // rpm
                                 SCRATCH_ALPHA, 
                                 SCRATCH_BETA, 
                                 true);
        }
    }
    else {
        engine.scratchDisable(this.deckNumber);
    }
};

/**
 * OMG YOU TURNED IT U R A DJ. Hooray?
 */
Deck.prototype.handleWheelTurn = function(channel, control, value, status, group) {
    var jogValue = value - 0x40; // Control centers on 0x40 (64)

    if (engine.isScratching(this.deckNumber)) {
        engine.scratchTick(this.deckNumber, jogValue);    // scratch
    }
    else {
        // Correct for wheel resolution if not scratching. (Scratching is 
        // corrected in the scratchEnable function)

        jogValue = jogValue * JOG_CORRECTION;
        engine.setValue(this.group, 'jog', jogValue);          // Pitch bend (or cue)
    }
};

/**
 * Handle "vinyl button" toggle
 */
Deck.prototype.handleVinyl = function(channel, control, value, status, group) {
    if (value === 0x7F) {           // Toggle on press, not on release
        this.vinylLed.toggle();
    }
};

/**
 * Sync button: sync tempo or (shifted) pitch? 
 */
Deck.prototype.handleSync = function(channel, control, value, status, group) {
    if (value === 0x7F) { // on button down

        if (this.globalState.isShifted) {

            // On shifted long-press, reset to the track's original key

            this.resetPitchTimer = 
                engine.beginTimer(500, function() {
                    this.resetPitchTimer = null;
                    engine.setValue(this.group, 'reset_key', 1);
                    engine.setValue(this.group, 'reset_key', 0);
                },
                true /* one-shot */ );
        }
        else {
            engine.setValue(this.group, 'beatsync', 1);
            engine.setValue(this.group, 'beatsync', 0);

            // On a long touch, become the beatsync master (or join group or whatever)

            this.beatsyncTimer = 
                engine.beginTimer(500, function() {
                    this.beatsyncTimer = null;
                    engine.setValue(this.group, 'sync_master', 1);  // join
                },
                true /* one-shot */ );
        }
    }
    else {                // button release
        // Check for "set sync master" and "reset pitch" timeout.
        // If a timer is still running, it means this was a "short press"

        if (this.beatsyncTimer) {
            engine.stopTimer(this.beatsyncTimer);
            this.beatsyncTimer = null;

            // On a "short press", leave the sync group. This should happen
            // quickly so we can make manual beatsync adjustments NOW.

            var mode = engine.getValue(this.group, 'sync_mode');
            if (mode >= 1) {      // already part of the sync group?
                engine.setValue(this.group, 'sync_mode', 0); // unsync
            }
        }

        if (this.resetPitchTimer) {
            engine.stopTimer(this.resetPitchTimer);
            this.resetPitchTimer = null;

            // on short-press, sync key

            engine.setValue(this.group, 'sync_key', 1);
            engine.setValue(this.group, 'sync_key', 0);
        }
    }
};

/**
 * Sync- button
 */
Deck.prototype.handleSyncMinus = function(channel, control, value, status, group) {
    if (value !== 0x7F) {
        return;
    }

    if (this.globalState.isShifted) {
        // pitch controls get twitchy if you don't button-up them
        engine.setValue(this.group, 'pitch_down', 1);
        engine.setValue(this.group, 'pitch_down', 0);
    }
    else {
        engine.setValue(this.group, 'beatjump_backward', 1);
        engine.setValue(this.group, 'beatjump_backward', 0);
    }
};

/**
 * Sync+ button
 */
Deck.prototype.handleSyncPlus = function(channel, control, value, status, group) {
    if (value !== 0x7F) {
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue(this.group, 'pitch_up', 1);
        engine.setValue(this.group, 'pitch_up', 0);
    }
    else {
        engine.setValue(this.group, 'beatjump_forward', 1);
        engine.setValue(this.group, 'beatjump_forward', 0);
    }
};

/**
 * Loop Auto button
 */
Deck.prototype.handleLoopAuto = function(channel, control, value, status, group) {
    //
    // Set/move beatloop.
    // If shifted: double loop size
    //

    if (value !== 0x7F) { // On button up
        return;           // do nothing
    }

    if (this.globalState.isShifted) {
        engine.setValue(this.group, 'loop_double', 1);
        engine.setValue(this.group, 'loop_double', 0);
    }
    else {
        engine.setValue(this.group, 'beatloop_activate', 1);
        engine.setValue(this.group, 'beatloop_activate', 0);
    }
};

/**
 * Loop in/out button
 */
Deck.prototype.handleLoopInOut = function(channel, control, value, status, group) {
    //
    // Create manual loops with one button.
    //   - First press sets loop-in
    //   - Second press sets loop-out and engages loop
    //   - If currently looping, disables loop
    //
    // If shifted:
    //   If loop-in is set but loop-out is not set: 
    //     clear loop-in (oops!)
    //   else 
    //     Halve loop size.
    //
    // Loop-in is harder than you think: "intelligently" decide
    // what to do based on whether a loop is active (beatloop or
    // otherwise), and whether loop-in and loop-out points are set.
    //
    // To make this work intelligently, we clear loop-out when setting
    // loop-in, using that as a flag that we have _only_ done the first
    // button press.
    //

    if (value !== 0x7F) { // On button up
        return;           // do nothing
    }

    if (this.globalState.isShifted) {
        engine.setValue(this.group, 'loop_halve', 1);
        engine.setValue(this.group, 'loop_halve', 0);
        return;
    }

    var isLoopActive = engine.getValue(this.group, 'loop_enabled');
    if (isLoopActive) {  // If a loop is active

        // Clear loop. (Should we clear loop-in/loop-out as well?)
        // engine.setValue(this.group, 'reloop_toggle', 0);

        engine.setValue(this.group, 'loop_end_position', -1);
        engine.setValue(this.group, 'loop_start_position', -1);
        return;
    }

    // No loop is active: 
    //   IF loop-in is not set
    //   OR (loop-in is set AND loop-out is set)
    //     Set loop-in, clear loop-out
    //   ELSE
    //     set loop-out and activate loop
    //
    // TODO: can't set loop-out less than loop-in. So if loop-in is 
    //       already set and you rewind the track, nothing will happen. :(

    var inPos = engine.getValue(this.group, 'loop_start_position');
    var outPos = engine.getValue(this.group, 'loop_end_position');
    if (inPos < 0 || outPos >= 0) {
        engine.setValue(this.group, 'loop_end_position', -1);
        engine.setValue(this.group, 'loop_in', 1);
        engine.setValue(this.group, 'loop_in', 0);
    }
    else {
        engine.setValue(this.group, 'loop_out', 1);
        engine.setValue(this.group, 'loop_out', 0);
    }
};

/**
 * The FX button
 */
Deck.prototype.handleFx = function(channel, control, value, status, group) {
    // Hey, one hotcue is better than zero!

    if (value !== 0x7f) {
        engine.setValue(this.group, 'hotcue_1_activate', 0); // prevent stuck activate
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue(this.group, 'hotcue_1_clear', 1);
        engine.setValue(this.group, 'hotcue_1_clear', 0);
    }
    else {
        engine.setValue(this.group, 'hotcue_1_activate', 1);
    }
};

/**
 * Main spin controller entry point
 */
const SpinController = function() {
    this.globalState = new GlobalState();
    this.deck1 = new Deck(this.globalState, '[Channel1]', 1);
    this.deck2 = new Deck(this.globalState, '[Channel2]', 2);
    this.vuLeft = new VuController('[Channel1]', 0);
    this.vuRight = new VuController('[Channel2]', 1);
};

SpinController.prototype.init = function(id) {
    engine.setValue("[Master]", "num_decks", 2);

    this.sendLightsOut();

    // Deck init
    this.deck1.init();
    this.deck2.init();
    this.vuLeft.init();
    this.vuRight.init();
};

SpinController.prototype.shutdown = function(id) {
    this.sendLightsOut();
};

SpinController.prototype.sendLightsOut = function() {
    // Raw midi init
    var i;
    for (i = 0; i < DECK_LEDS.length; i++) {
        midi.sendShortMsg(0x90, DECK_LEDS[i], 0);
        midi.sendShortMsg(0x91, DECK_LEDS[i], 0);
    }
    for (i = 0; i < CH3_LEDS.length; i++) {
        midi.sendShortMsg(0x92, CH3_LEDS[i], 0);
    }
};

SpinController.prototype.handleShift = function(channel, control, value, status, group) {
    if (value === 0x7f) {
        this.globalState.isShifted = true;
    }
    else {
        this.globalState.isShifted = false;
    }
};

/**
 * Handle nav up/down, shifted=page up/down
 */
SpinController.prototype.handleNavUp = function(channel, control, value, status, group) {
    if (value === 0x7f) {
        value = 1;
    }
    else {
        value = 0;
    }

    if (this.globalState.isShifted) {
        engine.setValue('[Library]', 'ScrollUp', value);
    }
    else {
        engine.setValue('[Library]', 'MoveUp', value);
    }
};

/**
 * Handle nav up/down, shifted=page up/down
 */
SpinController.prototype.handleNavDown = function(channel, control, value, status, group) {
    if (value !== 0x7f) {
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue('[Library]', 'ScrollDown', 1);
        engine.setValue('[Library]', 'ScrollDown', 0);
    }
    else {
        engine.setValue('[Library]', 'MoveDown', 1);
        engine.setValue('[Library]', 'MoveDown', 0);
    }
};

/**
 * Handle nav left/right, shifted = "Tab navigation"
 */
SpinController.prototype.handleNavLeft = function(channel, control, value, status, group) {
    if (value !== 0x7f) {
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue('[Library]', 'MoveFocusBackward', 1);
        engine.setValue('[Library]', 'MoveFocusBackward', 0);
    }
    else {
        engine.setValue('[Channel1]', 'LoadSelectedTrack', 1);
        engine.setValue('[Channel1]', 'LoadSelectedTrack', 0);
    }
};

SpinController.prototype.handleNavRight = function(channel, control, value, status, group) {
    if (value !== 0x7f) {
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue('[Library]', 'MoveFocusForward', 1);
        engine.setValue('[Library]', 'MoveFocusForward', 0);
    }
    else {
        engine.setValue('[Channel2]', 'LoadSelectedTrack', 1);
        engine.setValue('[Channel2]', 'LoadSelectedTrack', 0);
    }
};

SpinController.prototype.handleAutoDJ = function(channel, control, value, status, group) {
    if (value !== 0x7f) {
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue('[AutoDJ]', 'fade_now', 1);
        engine.setValue('[AutoDJ]', 'fade_now', 0);
    }
    else {
        script.toggleControl('[AutoDJ]', 'enabled');
    }
};

SpinController.prototype.handleMicOnOff = function(channel, control, value, status, group) {
    if (value !== 0x7f) {
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue('[Library]', 'AutoDjAddTop', 1);
        engine.setValue('[Library]', 'AutoDjAddTop', 0);
    }
    else {
        engine.setValue('[Library]', 'AutoDjAddBottom', 1);
        engine.setValue('[Library]', 'AutoDjAddBottom', 0);
    }
};

/**
 * Main entry point
 */
const Controller = new SpinController();

